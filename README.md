# HTML5 feature model diagram editor

Forked from [Evelance/feature_model_drawer](https://github.com/evelance/feature_model_drawer), with written permission.

A minimalistic feature model diagram editor... if it works for you, don't complain about the code quality ;)
Otherwise feel free to report bugs or improvements.

(You can use it here: https://eavise.gitlab.io/featuremodel/feature-model-editor/featuremodeldrawer.htm)

![Screenshot](screenshot.png "Example feature model")

## Changes

We have extended the model drawer with a few features after forking:

* Modified mousebinds to make more sense (left click = create, ...)
* Exporting to json
* Importing json
* Added a reset button
